package com.marketo.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Id;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "product")
public class ProductDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private double price;
    private String brand;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Column(name = "aditional_information", length=3000)
    private String additionalInfo;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST
            })
    @JoinTable(name = "product_tag",
            joinColumns = {@JoinColumn(name = "product_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id")})
    private List<TagDTO> tags = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="category_id")
    private CategoryDTO category;


    @OneToMany(fetch = FetchType.LAZY,mappedBy="product", cascade = CascadeType.ALL )
    private List<ImageDTO> images = new ArrayList<>();


    @OneToMany(fetch = FetchType.LAZY,mappedBy="product", cascade = CascadeType.ALL)
    private List<SpecsDTO> specs = new ArrayList<>();


    @ManyToMany(fetch = FetchType.LAZY,cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "product_subcategories",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "subcategory_id")
    )
    private List<SubCategoryDTO> subCategories = new ArrayList<>();

    public ProductDTO() {
    }

    public ProductDTO(String name, double price, String brand, String description, String additionalInfo, List<TagDTO> tags, CategoryDTO category, List<ImageDTO> images, List<SpecsDTO> specs, List<SubCategoryDTO> subCategories) {
        this.name = name;
        this.price = price;
        this.brand = brand;
        this.description = description;
        this.additionalInfo = additionalInfo;
        this.tags = tags;
        this.category = category;
        this.images = images;
        this.specs = specs;
        this.subCategories = subCategories;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CategoryDTO getCategory() {
        return category;
    }

    public void setCategory(CategoryDTO category) {
        this.category = category;
    }

    public List<ImageDTO> getImages() {
        return images;
    }

    public void setImages(List<ImageDTO> images) {
        this.images = images;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public List<TagDTO> getTags() {
        return tags;
    }

    public void setTags(List<TagDTO> tags) {
        this.tags = tags;
    }

    public List<SubCategoryDTO> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategoryDTO> subCategories) {
        this.subCategories = subCategories;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<SpecsDTO> getSpecs() {
        return specs;
    }

    public void setSpecs(List<SpecsDTO> specs) {
        this.specs = specs;
    }
}
