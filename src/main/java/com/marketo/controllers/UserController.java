package com.marketo.controllers;


import com.marketo.exception.ErrorResponse;
import com.marketo.models.User;
import com.marketo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/user")
@CrossOrigin
public class UserController {

    @Autowired
    UserService service;


    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody User user) {
        return this.service.isUserCredentials(user.getPassword(),user.getEmail()) ? new ResponseEntity(this.service.login(user), HttpStatus.OK) : new ResponseEntity<>(new ErrorResponse("The email or password is incorrect", false) ,HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/signup")
    public ResponseEntity<User> post(@RequestBody User user) {
        return this.service.isUserPresent(user.getEmail()) ? new ResponseEntity<>(HttpStatus.BAD_REQUEST) : new ResponseEntity<>(this.service.create(user), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<User>> getAll() {
        return new ResponseEntity<>(this.service.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getById(@PathVariable Long id) {
        return this.service.getById(id) != null ? new ResponseEntity<>(this.service.getById(id), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/find-by-email/{email}")
    public ResponseEntity<User> getByEmail(@PathVariable String email) {
        return this.service.finByEmail(email) != null ? new ResponseEntity<>(this.service.finByEmail(email), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping
    public ResponseEntity<User> update(@RequestBody User user, @RequestHeader(value="token") String token) {
        if(token == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }else{
           return new ResponseEntity<>(service.update(user), HttpStatus.OK);
        }
    }
}