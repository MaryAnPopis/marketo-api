package com.marketo.controllers;

import com.marketo.dto.ProductCardDTO;
import com.marketo.models.Product;
import com.marketo.models.StripePay;
import com.marketo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/v1/products")
@CrossOrigin
public class ProductController {

    @Autowired
    ProductService service;

    @GetMapping
    public ResponseEntity<Page<Product>> getAll(Pageable pageable) {
        return new ResponseEntity<>(this.service.getAll(pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getById(@PathVariable Long id) {
        return this.service.getById(id) != null ? new ResponseEntity<>(this.service.getById(id), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<Page<Product>> getByCategory(@PathVariable Long id,Pageable pageable) {
        return new ResponseEntity<>(this.service.getByCategory(id,pageable), HttpStatus.OK);
    }

    @PostMapping("/checkout-stripe")
    public ResponseEntity<StripePay> login(@RequestBody StripePay stripe) {
        return  new ResponseEntity<>(this.service.makePayment(stripe), HttpStatus.OK);
    }

    @GetMapping("/search/{keyword}")
    public ResponseEntity<List<ProductCardDTO>> search(@PathVariable String keyword) {
        return  new ResponseEntity(this.service.search(keyword), HttpStatus.OK);
    }
}
