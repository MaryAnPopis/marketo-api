package com.marketo.controllers;

import com.marketo.models.Category;
import com.marketo.models.SubCategory;
import com.marketo.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping("/v1/category")
@CrossOrigin
public class CategoryController {

    @Autowired
    CategoryService service;

    @GetMapping
    public ResponseEntity<List<Category>> getAll() {
        return new ResponseEntity<>(this.service.getAll(), HttpStatus.OK);
    }

    @GetMapping("/subcategories/{id}")
    public ResponseEntity<List<SubCategory>> getAllSubCategories(@PathVariable Long id) {
        return new ResponseEntity<>(this.service.getByCategory(id), HttpStatus.OK);
    }
}
