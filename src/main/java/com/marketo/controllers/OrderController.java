package com.marketo.controllers;

import com.marketo.exception.ErrorResponse;
import com.marketo.models.Order;
import com.marketo.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/order")
@CrossOrigin
public class OrderController {
    @Autowired
    OrderService service;

    @PostMapping("/send-email")
    public ResponseEntity<ErrorResponse> post(@RequestBody Order order) {
        ErrorResponse message =  this.service.sendEmail(order);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
}
