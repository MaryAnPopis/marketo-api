package com.marketo.models;

public class Specs {
    private Long id;
    private Product product;
    private String description;

    public Specs() {
    }

    public Specs(Long id, Product product, String description) {
        this.id = id;
        this.product = product;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
