package com.marketo.models;

public class StripePay {

    private String token;
    private String name;
    private String email;
    private double amount;

    private boolean isPaid;
    private String receipt;

    public StripePay() {
    }

    public StripePay(String token, String name, String email, double amount) {
        this.token = token;
        this.name = name;
        this.email = email;
        this.amount = amount;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
