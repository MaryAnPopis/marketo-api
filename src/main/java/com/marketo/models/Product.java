package com.marketo.models;


import java.util.ArrayList;
import java.util.List;

public class Product {

    private Long id;
    private String name;
    private double price;
    private String brand;
    private String description;
    private String additionalInfo;
    private List<Tag> tags = new ArrayList<>();
    private Category category;
    private List<Image> images = new ArrayList<>();
    private List<Specs> specs = new ArrayList<>();
    private List<SubCategory> subCategories = new ArrayList<>();

    public Product() {
    }
    public Product(String name, double price, String brand, String description, String additionalInfo, List<Tag> tags, Category category, List<Image> images, List<Specs> specs, List<SubCategory> subCategories) {
        this.name = name;
        this.price = price;
        this.brand = brand;
        this.description = description;
        this.additionalInfo = additionalInfo;
        this.tags = tags;
        this.category = category;
        this.images = images;
        this.specs = specs;
        this.subCategories = subCategories;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }

    public void setSpecs(List<Specs> specs) {
        this.specs = specs;
    }

    public List<Specs> getSpecs() {
        return specs;
    }
}
