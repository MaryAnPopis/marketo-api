package com.marketo.models;

import com.marketo.dto.ProductDTO;
import java.util.List;

public class Order {
    private String id;
    private String name;
    private String email;
    private List<OrderItem> productList;
    private double total;
    private String receipt;


    public Order() {
    }

    public Order(String id, String name, String email,  double total, String receipt) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.total = total;
        this.receipt = receipt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return name;
    }

    public void setUser(String user) {
        this.name = user;
    }

    public List<OrderItem> getProductList() {
        return productList;
    }

    public void setProductList(List<OrderItem> productList) {
        this.productList = productList;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

}
