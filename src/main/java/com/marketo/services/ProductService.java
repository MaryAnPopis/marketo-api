package com.marketo.services;

import com.marketo.dto.ProductCardDTO;
import com.marketo.models.Product;
import com.marketo.models.StripePay;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {
    Page<Product> getAll(Pageable pageable);
    Product getById(Long id);
    Page<Product> getByCategory(Long id,Pageable pageable );
    StripePay makePayment(StripePay stripe);
    List<ProductCardDTO> search(String keyword);
}
