package com.marketo.services.implementation;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.*;
import com.marketo.exception.ErrorResponse;
import com.marketo.models.Order;
import com.marketo.services.OrderService;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImp implements OrderService {

    static final String FROM = "dguezmari@gmail.com";
    static final String SUBJECT = "Thank you. Your order has been received.";
    static final String TEXTBODY = "Your order details";



    public ErrorResponse sendEmail(Order order){
        ErrorResponse message = new ErrorResponse();
        String HTMLBODY = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional //EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\"><head>\n" +
                "  <!--[if gte mso 9]><xml>\n" +
                "   <o:OfficeDocumentSettings>\n" +
                "    <o:AllowPNG/>\n" +
                "    <o:PixelsPerInch>96</o:PixelsPerInch>\n" +
                "   </o:OfficeDocumentSettings>\n" +
                "  </xml><![endif]-->\n" +
                "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width\">\n" +
                "  <!--[if !mso]><!--><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"><!--<![endif]-->\n" +
                "  <title></title>\n" +
                "  <!--[if !mso]><!-- -->\n" +
                "<link href=\"https://fonts.googleapis.com/css?family=Montserrat\" rel=\"stylesheet\" type=\"text/css\">\n" +
                "<link href=\"https://fonts.googleapis.com/css?family=Open+Sans\" rel=\"stylesheet\" type=\"text/css\">\n" +
                "<!--<![endif]-->\n" +
                "\n" +
                "  <style type=\"text/css\" id=\"media-query\">\n" +
                "    body {\n" +
                "margin: 0;\n" +
                "padding: 0; }\n" +
                "\n" +
                "table, tr, td {\n" +
                "vertical-align: top;\n" +
                "border-collapse: collapse; }\n" +
                "\n" +
                ".ie-browser table, .mso-container table {\n" +
                "table-layout: fixed; }\n" +
                "\n" +
                "* {\n" +
                "line-height: inherit; }\n" +
                "\n" +
                "a[x-apple-data-detectors=true] {\n" +
                "color: inherit !important;\n" +
                "text-decoration: none !important; }\n" +
                "\n" +
                "[owa] .img-container div, [owa] .img-container button {\n" +
                "display: block !important; }\n" +
                "\n" +
                "[owa] .fullwidth button {\n" +
                "width: 100% !important; }\n" +
                "\n" +
                "[owa] .block-grid .col {\n" +
                "display: table-cell;\n" +
                "float: none !important;\n" +
                "vertical-align: top; }\n" +
                "\n" +
                ".ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {\n" +
                "width: 700px !important; }\n" +
                "\n" +
                ".ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {\n" +
                "line-height: 100%; }\n" +
                "\n" +
                ".ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {\n" +
                "width: 232px !important; }\n" +
                "\n" +
                ".ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {\n" +
                "width: 464px !important; }\n" +
                "\n" +
                ".ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {\n" +
                "width: 350px !important; }\n" +
                "\n" +
                ".ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {\n" +
                "width: 233px !important; }\n" +
                "\n" +
                ".ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {\n" +
                "width: 175px !important; }\n" +
                "\n" +
                ".ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {\n" +
                "width: 140px !important; }\n" +
                "\n" +
                ".ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {\n" +
                "width: 116px !important; }\n" +
                "\n" +
                ".ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {\n" +
                "width: 100px !important; }\n" +
                "\n" +
                ".ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {\n" +
                "width: 87px !important; }\n" +
                "\n" +
                ".ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {\n" +
                "width: 77px !important; }\n" +
                "\n" +
                ".ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {\n" +
                "width: 70px !important; }\n" +
                "\n" +
                ".ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {\n" +
                "width: 63px !important; }\n" +
                "\n" +
                ".ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {\n" +
                "width: 58px !important; }\n" +
                "\n" +
                "@media only screen and (min-width: 720px) {\n" +
                ".block-grid {\n" +
                "  width: 700px !important; }\n" +
                ".block-grid .col {\n" +
                "  vertical-align: top; }\n" +
                "  .block-grid .col.num12 {\n" +
                "    width: 700px !important; }\n" +
                ".block-grid.mixed-two-up .col.num4 {\n" +
                "  width: 232px !important; }\n" +
                ".block-grid.mixed-two-up .col.num8 {\n" +
                "  width: 464px !important; }\n" +
                ".block-grid.two-up .col {\n" +
                "  width: 350px !important; }\n" +
                ".block-grid.three-up .col {\n" +
                "  width: 233px !important; }\n" +
                ".block-grid.four-up .col {\n" +
                "  width: 175px !important; }\n" +
                ".block-grid.five-up .col {\n" +
                "  width: 140px !important; }\n" +
                ".block-grid.six-up .col {\n" +
                "  width: 116px !important; }\n" +
                ".block-grid.seven-up .col {\n" +
                "  width: 100px !important; }\n" +
                ".block-grid.eight-up .col {\n" +
                "  width: 87px !important; }\n" +
                ".block-grid.nine-up .col {\n" +
                "  width: 77px !important; }\n" +
                ".block-grid.ten-up .col {\n" +
                "  width: 70px !important; }\n" +
                ".block-grid.eleven-up .col {\n" +
                "  width: 63px !important; }\n" +
                ".block-grid.twelve-up .col {\n" +
                "  width: 58px !important; } }\n" +
                "\n" +
                "@media (max-width: 720px) {\n" +
                ".block-grid, .col {\n" +
                "  min-width: 320px !important;\n" +
                "  max-width: 100% !important;\n" +
                "  display: block !important; }\n" +
                ".block-grid {\n" +
                "  width: calc(100% - 40px) !important; }\n" +
                ".col {\n" +
                "  width: 100% !important; }\n" +
                "  .col > div {\n" +
                "    margin: 0 auto; }\n" +
                "img.fullwidth, img.fullwidthOnMobile {\n" +
                "  max-width: 100% !important; }\n" +
                ".no-stack .col {\n" +
                "  min-width: 0 !important;\n" +
                "  display: table-cell !important; }\n" +
                ".no-stack.two-up .col {\n" +
                "  width: 50% !important; }\n" +
                ".no-stack.mixed-two-up .col.num4 {\n" +
                "  width: 33% !important; }\n" +
                ".no-stack.mixed-two-up .col.num8 {\n" +
                "  width: 66% !important; }\n" +
                ".no-stack.three-up .col.num4 {\n" +
                "  width: 33% !important; }\n" +
                ".no-stack.four-up .col.num3 {\n" +
                "  width: 25% !important; }\n" +
                ".mobile_hide {\n" +
                "  min-height: 0px;\n" +
                "  max-height: 0px;\n" +
                "  max-width: 0px;\n" +
                "  display: none;\n" +
                "  overflow: hidden;\n" +
                "  font-size: 0px; } }\n" +
                "\n" +
                "  </style>\n" +
                "</head>\n" +
                "<body class=\"clean-body\" style=\"margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF\">\n" +
                "<style type=\"text/css\" id=\"media-query-bodytag\">\n" +
                "  @media (max-width: 520px) {\n" +
                "    .block-grid {\n" +
                "      min-width: 320px!important;\n" +
                "      max-width: 100%!important;\n" +
                "      width: 100%!important;\n" +
                "      display: block!important;\n" +
                "    }\n" +
                "\n" +
                "    .col {\n" +
                "      min-width: 320px!important;\n" +
                "      max-width: 100%!important;\n" +
                "      width: 100%!important;\n" +
                "      display: block!important;\n" +
                "    }\n" +
                "\n" +
                "      .col > div {\n" +
                "        margin: 0 auto;\n" +
                "      }\n" +
                "\n" +
                "    img.fullwidth {\n" +
                "      max-width: 100%!important;\n" +
                "    }\n" +
                "    img.fullwidthOnMobile {\n" +
                "      max-width: 100%!important;\n" +
                "    }\n" +
                "    .no-stack .col {\n" +
                "      min-width: 0!important;\n" +
                "      display: table-cell!important;\n" +
                "    }\n" +
                "    .no-stack.two-up .col {\n" +
                "      width: 50%!important;\n" +
                "    }\n" +
                "    .no-stack.mixed-two-up .col.num4 {\n" +
                "      width: 33%!important;\n" +
                "    }\n" +
                "    .no-stack.mixed-two-up .col.num8 {\n" +
                "      width: 66%!important;\n" +
                "    }\n" +
                "    .no-stack.three-up .col.num4 {\n" +
                "      width: 33%!important;\n" +
                "    }\n" +
                "    .no-stack.four-up .col.num3 {\n" +
                "      width: 25%!important;\n" +
                "    }\n" +
                "    .mobile_hide {\n" +
                "      min-height: 0px!important;\n" +
                "      max-height: 0px!important;\n" +
                "      max-width: 0px!important;\n" +
                "      display: none!important;\n" +
                "      overflow: hidden!important;\n" +
                "      font-size: 0px!important;\n" +
                "    }\n" +
                "  }\n" +
                "</style>\n" +
                "<!--[if IE]><div class=\"ie-browser\"><![endif]-->\n" +
                "<!--[if mso]><div class=\"mso-container\"><![endif]-->\n" +
                "<table class=\"nl-container\" style=\"border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #FFFFFF;width: 100%\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                "<tbody>\n" +
                "<tr style=\"vertical-align: top\">\n" +
                "  <td style=\"word-break: break-word;border-collapse: collapse !important;vertical-align: top\">\n" +
                "  <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td align=\"center\" style=\"background-color: #FFFFFF;\"><![endif]-->\n" +
                "\n" +
                "  <div style=\"background-color:#FFFFFF;\">\n" +
                "    <div style=\"Margin: 0 auto;min-width: 320px;max-width: 700px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;\" class=\"block-grid \">\n" +
                "      <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\">\n" +
                "        <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"background-color:#FFFFFF;\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 700px;\"><tr class=\"layout-full-width\" style=\"background-color:transparent;\"><![endif]-->\n" +
                "\n" +
                "            <!--[if (mso)|(IE)]><td align=\"center\" width=\"700\" style=\" width:700px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><![endif]-->\n" +
                "          <div class=\"col num12\" style=\"min-width: 320px;max-width: 700px;display: table-cell;vertical-align: top;\">\n" +
                "            <div style=\"background-color: transparent; width: 100% !important;\">\n" +
                "            <!--[if (!mso)&(!IE)]><!--><div style=\"border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;\"><!--<![endif]-->\n" +
                "\n" +
                "\n" +
                "\n" +
                "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"divider \" style=\"border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%\">\n" +
                "  <tbody>\n" +
                "      <tr style=\"vertical-align: top\">\n" +
                "          <td class=\"divider_inner\" style=\"word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 0px;padding-left: 0px;padding-top: 0px;padding-bottom: 0px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%\">\n" +
                "              <table class=\"divider_content\" height=\"30px\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 0px solid transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%\">\n" +
                "                  <tbody>\n" +
                "                      <tr style=\"vertical-align: top\">\n" +
                "                          <td style=\"word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 30px;line-height: 30px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%\">\n" +
                "                              <span>&#160;</span>\n" +
                "                          </td>\n" +
                "                      </tr>\n" +
                "                  </tbody>\n" +
                "              </table>\n" +
                "          </td>\n" +
                "      </tr>\n" +
                "  </tbody>\n" +
                "</table>\n" +
                "\n" +
                "            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->\n" +
                "            </div>\n" +
                "          </div>\n" +
                "        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "  <div style=\"background-color:transparent;\">\n" +
                "    <div style=\"Margin: 0 auto;min-width: 320px;max-width: 700px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;\" class=\"block-grid two-up no-stack\">\n" +
                "      <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\">\n" +
                "        <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"background-color:transparent;\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 700px;\"><tr class=\"layout-full-width\" style=\"background-color:transparent;\"><![endif]-->\n" +
                "\n" +
                "            <!--[if (mso)|(IE)]><td align=\"center\" width=\"350\" style=\" width:350px; padding-right: 20px; padding-left: 20px; padding-top:10px; padding-bottom:10px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><![endif]-->\n" +
                "          <div class=\"col num6\" style=\"min-width: 320px;max-width: 350px;display: table-cell;vertical-align: top;\">\n" +
                "            <div style=\"background-color: transparent; width: 100% !important;\">\n" +
                "            <!--[if (!mso)&(!IE)]><!--><div style=\"border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:10px; padding-bottom:10px; padding-right: 20px; padding-left: 20px;\"><!--<![endif]-->\n" +
                "\n" +
                "\n" +
                "                  <div align=\"left\" class=\"img-container left fixedwidth \" style=\"padding-right: 0px;  padding-left: 0px;\">\n" +
                "<!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr style=\"line-height:0px;line-height:0px;\"><td style=\"padding-right: 0px; padding-left: 0px;\" align=\"left\"><![endif]-->\n" +
                "<img class=\"left fixedwidth\" align=\"left\" border=\"0\" src=\"https://i.imgur.com/XNxO6iZ.png\" alt=\"Image\" title=\"Image\" style=\"outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 155px\" width=\"155\">\n" +
                "<!--[if mso]></td></tr></table><![endif]-->\n" +
                "</div>\n" +
                "\n" +
                "\n" +
                "            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->\n" +
                "            </div>\n" +
                "          </div>\n" +
                "            <!--[if (mso)|(IE)]></td><td align=\"center\" width=\"350\" style=\" width:350px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><![endif]-->\n" +
                "          <div class=\"col num6\" style=\"min-width: 320px;max-width: 350px;display: table-cell;vertical-align: top;\">\n" +
                "            <div style=\"background-color: transparent; width: 100% !important;\">\n" +
                "            <!--[if (!mso)&(!IE)]><!--><div style=\"border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;\"><!--<![endif]-->\n" +
                "\n" +
                "\n" +
                "                  <div class=\"\">\n" +
                "<!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;\"><![endif]-->\n" +
                "<div style=\"color:#E3E3E3;font-family:'Open Sans', Helvetica, Arial, sans-serif;line-height:120%; padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;\">\n" +
                "  <div style=\"font-size:12px;line-height:14px;color:#E3E3E3;font-family:'Open Sans', Helvetica, Arial, sans-serif;text-align:left;\"><p style=\"margin: 0;font-size: 14px;line-height: 17px;text-align: right\"><span style=\"font-size: 12px; line-height: 14px;\">Your favorite ecommerce</span></p></div>\n" +
                "</div>\n" +
                "<!--[if mso]></td></tr></table><![endif]-->\n" +
                "</div>\n" +
                "\n" +
                "            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->\n" +
                "            </div>\n" +
                "          </div>\n" +
                "        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "  <div style=\"background-image:url('https://d1oco4z2z1fhwp.cloudfront.net/templates/default/286/Bg_text_ani.gif');background-position:top center;background-repeat:no-repeat;;background-color:#FFFFFF\">\n" +
                "    <div style=\"Margin: 0 auto;min-width: 320px;max-width: 700px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;\" class=\"block-grid \">\n" +
                "      <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\">\n" +
                "\n" +
                "          <div class=\"col num12\" style=\"min-width: 320px;max-width: 700px;display: table-cell;vertical-align: top;\">\n" +
                "            <div style=\"background-color: transparent; width: 100% !important;\">\n" +
                "            <!--[if (!mso)&(!IE)]><!--><div style=\"border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:40px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;\"><!--<![endif]-->\n" +
                "\n" +
                "\n" +
                "                  <div align=\"right\" class=\"img-container right fixedwidth \" style=\"padding-right: 0px;  padding-left: 0px;\">\n" +
                "</div>\n" +
                "\n" +
                "\n" +
                "            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->\n" +
                "            </div>\n" +
                "          </div>\n" +
                "        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "  <div style=\"background-image:url('https://d1oco4z2z1fhwp.cloudfront.net/templates/default/286/bg_wave_1.png');background-position:top center;background-repeat:repeat;;background-color:#F4F4F4\">\n" +
                "    <div style=\"Margin: 0 auto;min-width: 320px;max-width: 700px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;\" class=\"block-grid \">\n" +
                "      <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\">\n" +
                "        <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"background-image:url('https://d1oco4z2z1fhwp.cloudfront.net/templates/default/286/bg_wave_1.png');background-position:top center;background-repeat:repeat;;background-color:#F4F4F4\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 700px;\"><tr class=\"layout-full-width\" style=\"background-color:transparent;\"><![endif]-->\n" +
                "\n" +
                "            <!--[if (mso)|(IE)]><td align=\"center\" width=\"700\" style=\" width:700px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><![endif]-->\n" +
                "          <div class=\"col num12\" style=\"min-width: 320px;max-width: 700px;display: table-cell;vertical-align: top;\">\n" +
                "            <div style=\"background-color: transparent; width: 100% !important;\">\n" +
                "            <!--[if (!mso)&(!IE)]><!--><div style=\"border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;\"><!--<![endif]-->\n" +
                "\n" +
                "\n" +
                "\n" +
                "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"divider \" style=\"border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%\">\n" +
                "  <tbody>\n" +
                "      <tr style=\"vertical-align: top\">\n" +
                "          <td class=\"divider_inner\" style=\"word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 10px;padding-left: 10px;padding-top: 10px;padding-bottom: 10px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%\">\n" +
                "              <table class=\"divider_content\" height=\"70px\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 0px solid transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%\">\n" +
                "                  <tbody>\n" +
                "                      <tr style=\"vertical-align: top\">\n" +
                "                          <td style=\"word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 70px;line-height: 70px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%\">\n" +
                "                              <span>&#160;</span>\n" +
                "                          </td>\n" +
                "                      </tr>\n" +
                "                  </tbody>\n" +
                "              </table>\n" +
                "          </td>\n" +
                "      </tr>\n" +
                "  </tbody>\n" +
                "</table>\n" +
                "\n" +
                "            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->\n" +
                "            </div>\n" +
                "          </div>\n" +
                "        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "  <div style=\"background-color:#F4F4F4;\">\n" +
                "    <div style=\"Margin: 0 auto;min-width: 320px;max-width: 700px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;\" class=\"block-grid \">\n" +
                "      <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\">\n" +
                "        <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"background-color:#F4F4F4;\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 700px;\"><tr class=\"layout-full-width\" style=\"background-color:transparent;\"><![endif]-->\n" +
                "\n" +
                "            <!--[if (mso)|(IE)]><td align=\"center\" width=\"700\" style=\" width:700px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><![endif]-->\n" +
                "          <div class=\"col num12\" style=\"min-width: 320px;max-width: 700px;display: table-cell;vertical-align: top;\">\n" +
                "            <div style=\"background-color: transparent; width: 100% !important;\">\n" +
                "            <!--[if (!mso)&(!IE)]><!--><div style=\"border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;\"><!--<![endif]-->\n" +
                "\n" +
                "\n" +
                "                  <div class=\"\">\n" +
                "<!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 30px; padding-left: 30px; padding-top: 10px; padding-bottom: 0px;\"><![endif]-->\n" +
                "<div style=\"color:#555555;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:120%; padding-right: 30px; padding-left: 30px; padding-top: 10px; padding-bottom: 0px;\">\n" +
                "  <div style=\"font-size:12px;line-height:14px;font-family:Montserrat, 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;color:#555555;text-align:left;\"><p style=\"margin: 0;font-size: 14px;line-height: 17px\"><strong><span style=\"font-size: 46px; line-height: 55px;\">Hey, <span style=\"color: #666EE8; line-height: 55px; font-size: 46px;\">"+ order.getName() +"</span>!</span></strong></p></div>\n" +
                "</div>\n" +
                "<!--[if mso]></td></tr></table><![endif]-->\n" +
                "</div>\n" +
                "\n" +
                "\n" +
                "                  <div class=\"\">\n" +
                "<!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 30px; padding-left: 30px; padding-top: 15px; padding-bottom: 5px;\"><![endif]-->\n" +
                "<div style=\"color:#555555;font-family:'Open Sans', Helvetica, Arial, sans-serif;line-height:150%; padding-right: 30px; padding-left: 30px; padding-top: 15px; padding-bottom: 5px;\">\n" +
                "  <div style=\"font-size:12px;line-height:18px;color:#555555;font-family:'Open Sans', Helvetica, Arial, sans-serif;text-align:left;\"><p style=\"margin: 0;font-size: 12px;line-height: 18px\"><strong><span style=\"font-size: 20px; line-height: 30px;\">Thanks for your order. It’s on-hold until we confirm that payment has been received. In the meantime, here’s a reminder of what you ordered</span></strong></p></div>\n" +
                "</div>\n" +
                "<!--[if mso]></td></tr></table><![endif]-->\n" +
                "</div>\n" +
                "\n" +
                "\n" +
                "                  <div class=\"\">\n" +
                "<!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 30px; padding-left: 30px; padding-top: 15px; padding-bottom: 20px;\"><![endif]-->\n" +
                "<!--[if mso]></td></tr></table><![endif]-->\n" +
                "</div>\n" +
                "\n" +
                "            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->\n" +
                "            </div>\n" +
                "          </div>\n" +
                "        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "  <div style=\"background-color:#F4F4F4;\">\n" +
                "    <div style=\"Margin: 0 auto;min-width: 320px;max-width: 700px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;\" class=\"block-grid \">\n" +
                "      <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;\">\n" +
                "        <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"background-color:#F4F4F4;\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 700px;\"><tr class=\"layout-full-width\" style=\"background-color:#FFFFFF;\"><![endif]-->\n" +
                "\n" +
                "            <!--[if (mso)|(IE)]><td align=\"center\" width=\"700\" style=\" width:700px; padding-right: 5px; padding-left: 5px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><![endif]-->\n" +
                "          <div class=\"col num12\" style=\"min-width: 320px;max-width: 700px;display: table-cell;vertical-align: top;\">\n" +
                "            <div style=\"background-color: transparent; width: 100% !important;\">\n" +
                "            <!--[if (!mso)&(!IE)]><!--><div style=\"border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 5px; padding-left: 5px;\"><!--<![endif]-->\n" +
                "\n" +
                "\n" +
                "                  <div class=\"\">\n" +
                "<!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 10px; padding-left: 35px; padding-top: 25px; padding-bottom: 0px;\"><![endif]-->\n" +
                "<div style=\"color:#444444;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:120%; padding-right: 10px; padding-left: 35px; padding-top: 25px; padding-bottom: 0px;\">\n" +
                "  <div style=\"font-size:12px;line-height:14px;font-family:Montserrat, 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;color:#444444;text-align:left;\"><p style=\"margin: 0;font-size: 14px;line-height: 17px\"><span style=\"background-color: rgb(147, 245, 237); font-size: 14px; line-height: 16px;\"><span style=\"font-size: 24px; line-height: 28px; background-color: rgb(147, 245, 237);\"><span style=\"line-height: 28px; font-size: 24px; background-color: rgb(147, 245, 237);\">&#160;ORDER DETAILS:&#160;</span></span></span></p></div>\n" +
                "</div>\n" +
                "<!--[if mso]></td></tr></table><![endif]-->\n" +
                "</div>\n" +
                "\n" +
                "\n" +
                "                  <div class=\"\">\n" +
                "<!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 20px; padding-left: 30px; padding-top: 15px; padding-bottom: 30px;\"><![endif]-->\n" +
                "<div style=\"color:#555555;font-family:'Open Sans', Helvetica, Arial, sans-serif;line-height:180%; padding-right: 20px; padding-left: 30px; padding-top: 15px; padding-bottom: 30px;\">\n" +
                "  <div style=\"line-height:22px;font-size:12px;font-family:inherit;color:#555555;text-align:left;\"><ul style=\"font-size:12px;\">\n" +
                "\n" +
                "\n" +
                "      <li style=\"font-size: 14px; line-height: 25px;  list-style: none;\"><span style=\"font-size: 26px; line-height: 28px; list-style-type: none;\">ORDER NUMBER\n" +
                "&nbsp;&nbsp;&nbsp;&nbsp; <strong> #"+ order.getId() +" </strong>  </span></li>\n" +
                "      <br>\n" +
                "<br>\n" +
                "<li style=\"font-size: 14px; line-height: 25px;  list-style: none;\"><span style=\"font-size: 26px; line-height: 28px; list-style-type: none; list-style: none;\">TOTAL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $"+order.getTotal()+"  </span></li>\n" +
                "</ul></div>\n" +
                "</div>\n" +
                "<!--[if mso]></td></tr></table><![endif]-->\n" +
                "</div>\n" +
                "\n" +
             " <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->\n" +
                "            </div>\n" +
                "          </div>\n" +
                "        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "  <div style=\"background-color:#F4F4F4;\">\n" +
                "    <div style=\"Margin: 0 auto;min-width: 320px;max-width: 700px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;\" class=\"block-grid \">\n" +
                "      <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\">\n" +
                "        <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"background-color:#F4F4F4;\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 700px;\"><tr class=\"layout-full-width\" style=\"background-color:transparent;\"><![endif]-->\n" +
                "\n" +
                "            <!--[if (mso)|(IE)]><td align=\"center\" width=\"700\" style=\" width:700px; padding-right: 0px; padding-left: 0px; padding-top:25px; padding-bottom:60px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><![endif]-->\n" +
                "          <div class=\"col num12\" style=\"min-width: 320px;max-width: 700px;display: table-cell;vertical-align: top;\">\n" +
                "            <div style=\"background-color: transparent; width: 100% !important;\">\n" +
                "            <!--[if (!mso)&(!IE)]><!--><div style=\"border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:25px; padding-bottom:60px; padding-right: 0px; padding-left: 0px;\"><!--<![endif]-->\n" +
                "\n" +
                "\n" +
                "\n" +
                "<div align=\"center\" class=\"button-container center \" style=\"padding-right: 10px; padding-left: 10px; padding-top:10px; padding-bottom:10px;\">\n" +
                "<!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"><tr><td style=\"padding-right: 10px; padding-left: 10px; padding-top:10px; padding-bottom:10px;\" align=\"center\"><v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\"\" style=\"height:54pt; v-text-anchor:middle; width:188pt;\" arcsize=\"13%\" strokecolor=\"#3D3BEE\" fillcolor=\"#3D3BEE\"><w:anchorlock/><v:textbox inset=\"0,0,0,0\"><center style=\"color:#ffffff; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:26px;\"><![endif]-->\n" +
                "  <div style=\"color: #ffffff; background-color: #666EE8; border-radius: 9px; -webkit-border-radius: 9px; -moz-border-radius: 9px; max-width: 251px; width: 161px;width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 10px; padding-right: 45px; padding-bottom: 10px; padding-left: 45px; font-family: 'Open Sans', Helvetica, Arial, sans-serif; text-align: center; mso-border-alt: none;\">\n" +
                "    <span style=\"font-family:'Open Sans', Helvetica, Arial, sans-serif;font-size:16px;line-height:32px;\"><span style=\"font-size: 26px; line-height: 52px;\"><strong><a href=\"" +order.getReceipt()+ "\" style=\"text-decoration: none;  color: white;\" target=\"_blank\">STRIPE RECEIPT</a></strong></span></span>\n" +
                "  </div>\n" +
                "<!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->\n" +
                "</div>\n" +
                "\n" +
                "\n" +
                "            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->\n" +
                "            </div>\n" +
                "          </div>\n" +
                "        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "  <div style=\"background-image:url('https://d1oco4z2z1fhwp.cloudfront.net/templates/default/286/bg_wave_2.png');background-position:top center;background-repeat:repeat;;background-color:#F4F4F4\">\n" +
                "    <div style=\"Margin: 0 auto;min-width: 320px;max-width: 700px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;\" class=\"block-grid \">\n" +
                "      <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\">\n" +
                "        <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"background-image:url('https://d1oco4z2z1fhwp.cloudfront.net/templates/default/286/bg_wave_2.png');background-position:top center;background-repeat:repeat;;background-color:#F4F4F4\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 700px;\"><tr class=\"layout-full-width\" style=\"background-color:transparent;\"><![endif]-->\n" +
                "\n" +
                "            <!--[if (mso)|(IE)]><td align=\"center\" width=\"700\" style=\" width:700px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><![endif]-->\n" +
                "          <div class=\"col num12\" style=\"min-width: 320px;max-width: 700px;display: table-cell;vertical-align: top;\">\n" +
                "            <div style=\"background-color: transparent; width: 100% !important;\">\n" +
                "            <!--[if (!mso)&(!IE)]><!--><div style=\"border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;\"><!--<![endif]-->\n" +
                "\n" +
                "\n" +
                "\n" +
                "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"divider \" style=\"border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%\">\n" +
                "  <tbody>\n" +
                "      <tr style=\"vertical-align: top\">\n" +
                "          <td class=\"divider_inner\" style=\"word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 10px;padding-left: 10px;padding-top: 10px;padding-bottom: 10px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%\">\n" +
                "              <table class=\"divider_content\" height=\"70px\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 0px solid transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%\">\n" +
                "                  <tbody>\n" +
                "                      <tr style=\"vertical-align: top\">\n" +
                "                          <td style=\"word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 70px;line-height: 70px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%\">\n" +
                "                              <span>&#160;</span>\n" +
                "                          </td>\n" +
                "                      </tr>\n" +
                "                  </tbody>\n" +
                "              </table>\n" +
                "          </td>\n" +
                "      </tr>\n" +
                "  </tbody>\n" +
                "</table>\n" +
                "\n" +
                "            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->\n" +
                "            </div>\n" +
                "          </div>\n" +
                "        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "  <div style=\"background-color:#FFFFFF;\">\n" +
                "    <div style=\"Margin: 0 auto;min-width: 320px;max-width: 700px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;\" class=\"block-grid \">\n" +
                "      <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;\">\n" +
                "        <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"background-color:#FFFFFF;\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width: 700px;\"><tr class=\"layout-full-width\" style=\"background-color:#FFFFFF;\"><![endif]-->\n" +
                "\n" +
                "            <!--[if (mso)|(IE)]><td align=\"center\" width=\"700\" style=\" width:700px; padding-right: 0px; padding-left: 0px; padding-top:15px; padding-bottom:35px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><![endif]-->\n" +
                "          <div class=\"col num12\" style=\"min-width: 320px;max-width: 700px;display: table-cell;vertical-align: top;\">\n" +
                "            <div style=\"background-color: transparent; width: 100% !important;\">\n" +
                "            <!--[if (!mso)&(!IE)]><!--><div style=\"border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:15px; padding-bottom:35px; padding-right: 0px; padding-left: 0px;\"><!--<![endif]-->\n" +
                "\n" +
                "\n" +
                "                  <div class=\"\">\n" +
                "\n" +
                "<div style=\"color:#838383;font-family:'Open Sans', Helvetica, Arial, sans-serif;line-height:150%; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;\">\n" +
                "  <div style=\"font-size:12px;line-height:18px;color:#838383;font-family:'Open Sans', Helvetica, Arial, sans-serif;text-align:left;\"><p style=\"margin: 0;font-size: 14px;line-height: 21px;text-align: center\"><span style=\"color: rgb(0, 0, 0); font-size: 14px; line-height: 21px;\"><strong>MARKETO</strong></span>, your favorite ecommerce.</p></div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</body></html>";
        String TO = order.getEmail();
        try {
            AmazonSimpleEmailService client =
                    AmazonSimpleEmailServiceClientBuilder.standard()
                            // Replace US_WEST_2 with the AWS Region you're using for
                            // Amazon SES.
                            .withRegion(Regions.US_WEST_2).build();
            SendEmailRequest request = new SendEmailRequest()
                    .withDestination(
                            new Destination().withToAddresses(TO))
                    .withMessage(new Message()
                            .withBody(new Body()
                                    .withHtml(new Content()
                                            .withCharset("UTF-8").withData(HTMLBODY))
                                    .withText(new Content()
                                            .withCharset("UTF-8").withData(TEXTBODY)))
                            .withSubject(new Content()
                                    .withCharset("UTF-8").withData(SUBJECT)))
                    .withSource(FROM);
            client.sendEmail(request);
            System.out.println("Email sent!");
            message = new ErrorResponse("Email sent!", true);
        } catch (Exception ex) {
            message = new ErrorResponse("The email was not sent!", false);
            System.out.println("The email was not sent. Error message: "
                    + ex.getMessage());
        }

return message;
    }
}
