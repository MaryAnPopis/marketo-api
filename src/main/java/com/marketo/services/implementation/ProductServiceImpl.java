package com.marketo.services.implementation;

import com.marketo.dao.IProductRepository;
import com.marketo.dto.CategoryDTO;
import com.marketo.dto.ProductCardDTO;
import com.marketo.dto.ProductDTO;
import com.marketo.models.Category;
import com.marketo.models.Product;
import com.marketo.models.StripePay;
import com.marketo.services.ProductService;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private IProductRepository repository;

    private ModelMapper modelMapper = new ModelMapper();

    @Transactional(readOnly = true)
    public Page<Product> getAll(Pageable pageable){
        Page<ProductCardDTO> dto = this.repository.findAllCustom(pageable);
        Type listType = new TypeToken<Page<Product>>() {}.getType();
        return modelMapper.map(dto, listType);
    }

    @Transactional(readOnly = true)
    public Product getById(Long id){
        Optional<ProductDTO> productDTO = this.repository.findById(id);
        Product product = null;
        if(productDTO.isPresent()){
            product = modelMapper.map(productDTO.get(), Product.class);
        }

        return product;
    }

    @Transactional(readOnly = true)
    public Page<Product> getByCategory(Long id,Pageable pageable ){
        Page<ProductCardDTO> dto = this.repository.findByCategory(new CategoryDTO(id),pageable);
        Type listType = new TypeToken<Page<Product>>() {}.getType();
        return modelMapper.map(dto, listType);
    }


    @Transactional()
    public StripePay makePayment(StripePay stripe){
        Stripe.apiKey = "sk_test_6dSJzcsyLk7s6EtSZselSigo"; // Secret key
        int amount = (int)stripe.getAmount() * 100;
        Map<String, Object> chargeMap = new HashMap<>();
        chargeMap.put("amount", amount);
        chargeMap.put("currency", "usd");
        chargeMap.put("source", stripe.getToken()); // obtained via Stripe.js

        try {
            Charge charge  = Charge.create(chargeMap);
            stripe.setPaid(charge.getPaid());
            stripe.setReceipt(charge.getReceiptUrl());
        } catch (StripeException e) {
            e.printStackTrace();
        }


        return stripe;
    }

    public List<ProductCardDTO> search(String keyword) {
        List<ProductCardDTO> dto = this.repository.search(keyword);
        return dto;
    }
}
