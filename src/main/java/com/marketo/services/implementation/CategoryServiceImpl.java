package com.marketo.services.implementation;

import com.marketo.dao.ICategoryRepository;
import com.marketo.dao.ISubCategoryRepository;
import com.marketo.dto.CategoryDTO;
import com.marketo.dto.SubCategoryDTO;
import com.marketo.models.Category;
import com.marketo.models.SubCategory;
import com.marketo.services.CategoryService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private ICategoryRepository repository;

    @Autowired
    private ISubCategoryRepository subRepository;


    private ModelMapper modelMapper = new ModelMapper();

    @Transactional(readOnly = true)
    public List<Category> getAll(){
        List<CategoryDTO> dto = this.repository.findAll();
        Type listType = new TypeToken<List<Category>>() {}.getType();
        return modelMapper.map(dto, listType);
    }

    public List<SubCategory> getByCategory(Long id){
        List<SubCategoryDTO> dto = this.subRepository.findByCategory(new CategoryDTO(id));
        Type listType = new TypeToken<List<SubCategory>>() {}.getType();
        return modelMapper.map(dto, listType);
    }

}
