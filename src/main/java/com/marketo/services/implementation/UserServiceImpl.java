package com.marketo.services.implementation;

import com.auth0.jwt.exceptions.JWTCreationException;
import com.marketo.dao.IUserRepository;
import com.marketo.dto.UserDTO;
import com.marketo.utils.Token;
import com.marketo.models.User;
import com.marketo.services.UserService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private IUserRepository repository;

    private ModelMapper modelMapper = new ModelMapper();


    private String getSecurePassword(String passwordToHash){
        String generatedPassword = "";
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(passwordToHash.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    /**
     * If user is in database will return true
     * @param email user email to look on database
     * @return boolean
     */
    public boolean isUserPresent(String email){
        return repository.findByEmail(email) != null;
    }

    public boolean isUserCredentials(String password, String email){
        User userByEmail = modelMapper.map(repository.findByEmail(email), User.class);

        return userByEmail.getPassword().equals(getSecurePassword(password)) && userByEmail.getEmail().equals(email);
    }

    @Transactional()
    public Token login(User user){
        String token = "";
        if(!user.getEmail().equals("") && !user.getPassword().equals("")){
            User userByEmail = modelMapper.map(repository.findByEmail(user.getEmail()), User.class);
        if(isUserCredentials(user.getPassword(), user.getEmail())){
            try {
                token = Token.get(userByEmail.getName(),userByEmail.getEmail(), userByEmail.getId() );
            } catch (JWTCreationException  | UnsupportedEncodingException exception){
                exception.printStackTrace();
            }
        }else{
            token = null;
        }
        }else{
            token = null;
        }
        return new Token(token);
    }

    @Transactional()
    public User create(User user){
        if(repository.findByEmail(user.getEmail()) != null){
            return null;
        }else{
            UserDTO userDTO = modelMapper.map(user, UserDTO.class);
            String encryptedPass = getSecurePassword(userDTO.getPassword());
            userDTO.setPassword(encryptedPass);
            UserDTO userCreated = repository.save(userDTO);
            return new User(userCreated.getId(), userCreated.getName(),userCreated.getLastName(), userCreated.getEmail(), userCreated.getAddress());
        }
    }

    @Transactional(readOnly = true)
    public List<User> getAll(){
        List<UserDTO> dto = this.repository.findAll();
        Type listType = new TypeToken<List<User>>() {}.getType();
        return modelMapper.map(dto, listType);
    }

    @Transactional(readOnly = true)
    public User getById(Long id){
        Optional<UserDTO> userDTO = this.repository.findById(id);
        User user = null;
        if(userDTO.isPresent()){
            user = modelMapper.map(userDTO.get(), User.class);
        }

        return user;
    }

    @Transactional()
    public User update(User newUser) {
        Optional<UserDTO> userFound = this.repository.findById(newUser.getId());

        if(userFound.isPresent()){
            UserDTO userDTO = userFound.get();

            userDTO.setName(newUser.getName());
            userDTO.setLastName(newUser.getLastName());
            userDTO.setPassword(newUser.getPassword());
            userDTO.setAddress(newUser.getAddress());
            userDTO.setEmail(newUser.getEmail());
            userDTO.setAdmin(newUser.getIsAdmin());

            return modelMapper.map(this.repository.save(userDTO), User.class);
        }
        return modelMapper.map(userFound, User.class);
    }

    public User finByEmail(String email){
        UserDTO userDTO = repository.findByEmail(email);
        UserDTO modelUser = new UserDTO();
        modelUser.setName(userDTO.getName());
        modelUser.setId(userDTO.getId());
        modelUser.setEmail(userDTO.getEmail());
        modelUser.setAddress(userDTO.getAddress());
        modelUser.setLastName(userDTO.getLastName());
        modelUser.setPassword(userDTO.getPassword());
        return modelMapper.map(modelUser,User.class );
    }
}
