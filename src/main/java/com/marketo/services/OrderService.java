package com.marketo.services;

import com.marketo.exception.ErrorResponse;
import com.marketo.models.Order;

public interface OrderService  {

    ErrorResponse sendEmail(Order order);

}
