package com.marketo.services;

import com.marketo.models.Category;
import com.marketo.models.SubCategory;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();
    List<SubCategory> getByCategory(Long id);
}
