package com.marketo.services;

import com.marketo.utils.Token;
import com.marketo.models.User;

import java.util.List;

public interface UserService {
    boolean isUserCredentials(String password, String email);
    boolean isUserPresent(String email);
    Token login(User user);
    User create(User user);
    List<User> getAll();
    User getById(Long id);
    User update(User newUser);
    User finByEmail(String email);
}
