package com.marketo.configuration;

import com.marketo.utils.Interceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new Interceptor())
                .excludePathPatterns("/v1/user/login")
                .excludePathPatterns("/v1/user/*/**")
                .excludePathPatterns("/v1/user")
                .excludePathPatterns("/v1/user/*")
                .excludePathPatterns("/v1/products")
                .excludePathPatterns("/v1/order/*")
                .excludePathPatterns("/v1/category")
                .excludePathPatterns("/v1/products/*")
                .excludePathPatterns("/v1/products/*/**/***")
                .excludePathPatterns("/v1/products/*/**")
                .excludePathPatterns("/v1/category/*")
                .excludePathPatterns("/v1/category/*/**")
                .excludePathPatterns("/v1/user/signup");
    }
}
