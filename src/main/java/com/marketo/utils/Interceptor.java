package com.marketo.utils;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketo.exception.InvalidToken;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

public class Interceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        String token = request.getHeader("token");
        boolean isValid = false;

        try {
            if(!token.equals("")){
                DecodedJWT jwt = Token.verify(token);
                isValid = true;
            }
        } catch (JWTVerificationException | UnsupportedEncodingException exception){
            ObjectMapper mapper = new ObjectMapper();
            InvalidToken exceptionToken = new InvalidToken(exception.toString(), false);
            String Json =  mapper.writeValueAsString(exceptionToken);
            response.setContentType("application/json");
            response.getWriter().write(Json);
            response.setStatus(405);
        }

        return isValid;
    }
}
