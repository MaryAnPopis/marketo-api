package com.marketo.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

import java.io.UnsupportedEncodingException;
import java.util.Date;

public class Token {
    private String token;

    private static String SECRET = "c3bff416-993f-4760-9275-132b00256944";

    public Token() {
    }

    public static String get(String name, String email, Long id) throws UnsupportedEncodingException {
        return JWT.create()
                .withIssuer("auth0")
                .withClaim("name", name)
                .withClaim("email", email)
                .withClaim("id", id)
                .withExpiresAt(new Date(System.currentTimeMillis() + (60 * 60 * 1000)))
                .sign(Algorithm.HMAC256(Token.SECRET));
    }

    public static DecodedJWT verify(String token) throws JWTVerificationException, UnsupportedEncodingException {
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(Token.SECRET))
                .withIssuer("auth0")
                .acceptExpiresAt(59)
                .build();

        return verifier.verify(token);
    }

    public Token(String returnMessage) {
        this.token = returnMessage;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
