package com.marketo.dao;

import com.marketo.dto.UserDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository  extends JpaRepository<UserDTO, Long> {
    UserDTO findByEmail(String email);
}
