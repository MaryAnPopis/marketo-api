package com.marketo.dao;

import com.marketo.dto.CategoryDTO;
import com.marketo.dto.SubCategoryDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ISubCategoryRepository  extends JpaRepository<SubCategoryDTO, Long> {

    List<SubCategoryDTO> findByCategory(CategoryDTO id);
}
