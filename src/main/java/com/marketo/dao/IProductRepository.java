package com.marketo.dao;


import com.marketo.dto.CategoryDTO;
import com.marketo.dto.ProductCardDTO;
import com.marketo.dto.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;

public interface IProductRepository  extends JpaRepository<ProductDTO, Long> {

    @Query( "select new com.marketo.dto.ProductCardDTO( p.id, p.name, p.price, pm.url ) from ProductDTO p, ImageDTO pm  where pm.product = p.id  group by p.id" )
    Page<ProductCardDTO> findAllCustom(Pageable pageable );

    @Query( "select new com.marketo.dto.ProductCardDTO( p.id, p.name, p.price, pm.url ) from ProductDTO p, ImageDTO pm where category_id = :id AND pm.product = p.id  group by p.id" )
    Page<ProductCardDTO> findByCategory(CategoryDTO id,Pageable pageable );


    @Query("select new com.marketo.dto.ProductCardDTO( p.id, p.name, p.price, pm.url ) from ProductDTO p, ImageDTO pm where p.name like :keyword% AND pm.product = p.id  group by p.id")
    List<ProductCardDTO> search(@Param("keyword") String keyword);
}
